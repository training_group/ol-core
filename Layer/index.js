import { Vector as VectorLayer } from 'ol/layer';
import { Vector as VectorSource } from 'ol/source';

class Layer {
  constructor(map) {
    this.map = map;

    this.create = this.create.bind(this);
    this.addFeature = this.addFeature.bind(this);
    this.addFeatures = this.addFeatures.bind(this);
    this.get = this.get.bind(this);
    this.removeFeature = this.removeFeature.bind(this);
    this.layers = {}
  }

  create(nameLayer) {
    let self = this;
    let source = new VectorSource({
      features: [],
      projection: 'EPSG:3857'
    });

    let vector = new VectorLayer({
      source: source
    });

    self.layers[nameLayer || 'default'] = { source, vector };

    return { source, vector }
  }

  get(nameLayer) {
    let self = this;
    const layer = self.layers[nameLayer];
    if (layer) {
      return layer
    }
    return null;
  }

  addFeature(nameLayer, feature) {
    let self = this;
    if (!feature) {
      return null;
    }
    const layer = self.get(nameLayer);
    if (layer) {
      layer.source.addFeature(feature);
      return layer
    }
    return null;
  }

  addFeatures(nameLayer, features) {
    let self = this;
    if (!features) {
      return null;
    }
    const layer = self.get(nameLayer);
    if (layer) {
      layer.source.addFeatures(features);
      return layer
    }
    return null;
  }

  removeFeature(nameLayer, feature) {
    let self = this;
    if (!features) {
      return null;
    }
    const layer = self.get(nameLayer);
    if (layer) {
      layer.source.removeFeature(feature);
      return layer
    }
    return null;
  }
}

export default Layer;