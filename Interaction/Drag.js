import DragAndDrop from 'ol/interaction/DragAndDrop';

class Drag {
  constructor(){
    this.init = this.init.bind(this);
  }

  init(source) {
    return new DragAndDrop({
      source
    });
  }
}

export default Drag;