import Draw from './Draw'
import Modify from "./Modify";
import Drag from "./Drag";
import Select from "./Select";
import Translate from "./Translate";

class Interaction {
  constructor(map) {
    this.map = map;
    this.interactions = {};
    this.draw = new Draw();
    this.modify = new Modify();
    this.drag = new Drag();
    this.select = new Select();
    this.translate = new Translate();

    this.add = this.add.bind(this);
    this.remove = this.remove.bind(this);
  }

  add(interaction, name) {
    let self = this;
    self.map.addInteraction(interaction);

    self.interactions[name] = interaction;
  }

  remove(name) {
    let self = this;
    const interaction = self.interactions[name];
    if (!interaction) {
      return null;
    }

    self.map.removeInteraction(interaction);
    return true;
  }
}

export default Interaction;