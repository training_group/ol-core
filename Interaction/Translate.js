import { Translate as TranslateOl } from 'ol/interaction';

class Translate {
  constructor() {
    this.init = this.init.bind(this);
  }

  init(layers) {
    return new TranslateOl({layers});
  }
}

export default Translate;