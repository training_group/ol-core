import {Draw as DrawOl} from 'ol/interaction';

class Draw {
  constructor() {
    this.init = this.init.bind(this);
  }

  init(source, type) {
    return new DrawOl({
      source,
      type
    });
  }
}

export default Draw;