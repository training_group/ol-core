import { Modify as ModifyOl} from 'ol/interaction';

class Modify {
  constructor() {
    this.init = this.init.bind(this);
  }
  init(source){
    return new ModifyOl({source: source});
  }
}

export default Modify;