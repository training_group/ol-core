import SelectOl from 'ol/interaction/Select';
import { click } from 'ol/events/condition';

class Select {
  constructor() {
    this.init = this.init.bind(this);
  }

  init(layers) {
    return new SelectOl({ condition: click, layers: layers})
  }
}

export default Select;