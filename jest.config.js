module.exports = {
  "globals": {
    "NODE_ENV": "test"
  },
  "verbose": true,
  "automock": true,
  "moduleFileExtensions": [
    "js",
    "json",
    "es6"
  ],
  "testPathIgnorePatterns": [
    "/node_modules/"
  ],
  "transform": {
    "^.+\\.js?$": "babel-jest",
    "^.+\\test.js?$": "babel-jest",
  }
};