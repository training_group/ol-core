import { Map as MapOl, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import { OSM } from 'ol/source';


class Map {
  constructor() {
    this.create = this.create.bind(this);
    this.addLayer = this.addLayer.bind(this);
    this.removeLayer = this.removeLayer.bind(this);
  }

  create(target) {
    let self = this;
    const map = new MapOl({
      target: target || 'map',
      layers: [
        new TileLayer({
          source: new OSM()
        })
      ],
      view: new View({
        center: [0, 0],
        zoom: 2
      })
    });
    self.map = map;
    return map;
  }

  addLayer(vector) {
    let self = this;
    if (!vector) {
      return null
    }
    self.map.addLayer(vector)
  }

  removeLayer(vector) {
    let self = this;
    if (!vector) {
      return null
    }
    self.map.removeLayer(vector)
  }
}

export default Map;