import Map from "../Map"
import Layer from "../Layer"
import Interaction from '../Interaction'
import Features from '../Features';

import Utils from '../Utils/index.js'


class Core {
  constructor(target) {
    this.initMap(target);
    this.initLayer();
    this.initInteraction();
    this.initFeatures();

    this.utils = Utils
  }

  initMap(target) {
    let self = this;
    const _map = new Map();
    self.map = _map.create(target);
  }

  initLayer() {
    let self = this;
    self.layer = new Layer(self.map)
  }

  initInteraction() {
    let self = this;
    self.interaction = new Interaction(self.map)
  }

  initFeatures() {
    let self = this;
    self.features = new Features()
  }
}

export { Core };