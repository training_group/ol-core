import GeoJSON from 'ol/format/GeoJSON';

function geoJsonToFeature(geoJson) {
  if (!geoJson) {
    return null;
  }
  return new GeoJSON().readFeature(geoJson);
}

function geoJsonToFeatures(geoJson) {
  if (!geoJson) {
    return null;
  }
  return new GeoJSON().readFeatures(geoJson);
}

function featuresToGeoJson(features) {
  if (!features) {
    return null;
  }
  return new GeoJSON().writeFeaturesObject(features);
}

export default {
  geoJsonToFeature,
  geoJsonToFeatures,
  featuresToGeoJson
};