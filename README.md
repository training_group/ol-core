# API ol-core

## Оглавление

1. [Core](#core)
2. [Map](#map)
3. [Layer](#layer)
4. [Features](#features)
5. [Interaction](#interaction)
    1. [List](#list)
6. [Utils](#utils)


## Core

Для работы с API ol-core необходимо подключить файл со сборкой к Вашему проекту.  
**Пример**  
```
<script src="../assets/geo.bundle.js"></script>
<script>
     let core = new gisApi.Core('map');
</script>
```
Вызов
```javascript
new gisApi.Core([target]);
```

* `target` - id div куда вставлять карту. Поумолчанию 'map'

Return - ``{map, layer, interaction, utils}``

## Map
***map.addLayer(layer)*** 

Добавление слоя на карту

* ``layer`` - Векторный слой полученный при [создании слоя](#layer)

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.map.addLayer(vector);
```

____
***map.removeLayer(layer)*** 

Удаление слоя с карты

* ``layer`` - Векторный слой полученный при [создании слоя](#layer)

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.map.removeLayer(vector);
```

## Layer
***layer.create(nameLayer)***

Создание слоя

* ``nameLayer`` - Название слоя. При отсутствие названия он будет создан с `default` названием. Если название совподает, то произойдет перезапись слоя.

Return - ``{source, vector}``

____

***layer.get(nameLayer)***

Получение слоя

* ``nameLayer`` - Название слоя.

Return - ``{source, vector}`` или ``null``

____

***layer.addFeature(nameLayer, feature)***

Добавление фичи

* ``nameLayer`` - Название слоя.
* ``feature`` - Векторный объект для географических объектов с геометрией и другими свойствами атрибутов, аналогичными объектам в векторных форматах файлов, таких как GeoJSON. Для получения `feature`  из GeoJSON см. [тут](#Convert).

Return - ``{source, vector}`` или ``null``

Пример: 
````
core.layer.addFeature(nameLayer, feature);
````
____

***layer.addFeatures(nameLayer, features)***

Добавление фич

* ``nameLayer`` - Название слоя.
* ``features`` - Векторный объект. Для получения `features`  из GeoJSON см. [тут](#convert).

Return - ``{source, vector}`` или ``null``

Пример: 
````
core.layer.addFeatures(nameLayer, features);
````
____

***layer.removeFeature(nameLayer, feature)***

Удаление фичи

* ``nameLayer`` - Название слоя.
* ``feature`` - Векторный объект. Для получения `features`  из GeoJSON см. [тут](#convert).

Return - ``{source, vector}`` или ``null``

Пример: 
````
core.layer.removeFeature(nameLayer, feature);
````

## Features
***features.add(layer, features[])***

Добавление фич на слой

* ``layer`` - Векторный слой полученный при [создании слоя](#layer).
* ``features`` - Массив фич.

Return - ``{source, vector}`` или ``null``
____

***features.remove(layer, features[])***

Удаление фич со слоя

* ``layer`` - Векторный слой полученный при [создании слоя](#layer).
* ``features`` - Массив фич.

Return - ``{source, vector}`` или ``null``

## Interaction

***interaction.add(interaction, name)***

Добавление взаимодействия с `features` 

* ``interaction`` - Взаимодействие. См. [список](#list) возможных взаимодействий.
* ``name`` - Название.

Return - ``null``

Пример:
```
core.interaction.add(select, 'select');
```
____

***interaction.remove(name)***

Удаление взаимодействия с `features`

* ``name`` - Название.

Return - ``true`` или ``null``
____

#### List

***draw.init(source, type)***

* ``source`` - Источник полученный из слоя.
* ``type`` - Тип геометрии. `'Point', 'LineString', 'LinearRing', 'Polygon', 'MultiPoint', 'MultiLineString', 'MultiPolygon', 'GeometryCollection', 'Circle'`

Return - ``interaction - draw``

Пример:
```
const draw = core.interaction.draw.init(source, 'Circle');
```
____

***modify.init(source)***

* ``source`` - Источник полученный из слоя.

Return - ``interaction - modify``

Пример:
```
const modify = core.interaction.modify.init(source);
```
____

***select.init([layers[]])***

* ``*layers[]`` - Массив слоев. Если не указывать, то будет распростроняться на все слои.

Return - ``interaction - select``

Пример:
```
const select = core.interaction.select.init([vector]);
```
____

***translate.init(vector[])***

* ``vector[]`` - Массив векторов слоев с которыми можно взаимодействовать.

Return - ``interaction - translate``

Пример:
```
const translate = core.interaction.translate.init([vector]);
```
____

## Utils

### Convert

***Converter.geoJsonToFeatures(geoJson)***

Преобразование GeoJson в features 

* ``geoJson`` - GeoJson

Return - ``features``

Пример:
```
let geojsonObject = {
      'type': 'FeatureCollection',
      'crs': {
        'type': 'name',
        'properties': {
          'name': 'EPSG:3857'
        }
      },
      'features': [{
        'type': 'Feature',
        'geometry': {
          'type': 'Point',
          'coordinates': [155555, 155555]
        }
      }]
    };

let features = core.utils.Converter.geoJsonToFeatures(geojsonObject);
```

