class Features {
  constructor() {
    this.add = this.add.bind(this);
    this.remove = this.remove.bind(this);
  }

  add(layer, features) {
    if (!feature) {
      return null;
    }
    if (layer) {
      layer.source.addFeatures(features);
      return layer
    }
    return null;
  }

  remove(layer, features) {
    if (!features) {
      return null;
    }
    if (layer) {
      for (let i = 0; i < features.length; i++) {
        layer.source.removeFeature(features[i]);
      }
      return layer
    }
    return null;
  }
}

export default Features;
